"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from restapi import views


router = routers.DefaultRouter()
router.register(r'taskList', views.TaskListViewSet)
router.register(r'tags', views.TagViewSet)
router.register(r'tasks', views.TaskViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
#urlpatterns = [
#    path('admin/', admin.site.urls),
#]

"""
    /taskList: retorna todas as lista cadastradas e permite criar uma nova lista. Cada lista possui zero ou mais tarefas.
    /taskList/id: permite a edição, alteração e remoção de uma lista específica
    /tasks: retorna todas as tarefas de uma lista e permite criar uma nova tarefa. Cada tarefa esta sempre associada à uma lista.
    /tasks/id: permite a edição, alteração e remoção de uma tarefa
    /tags: retorna todas as tags cadastradas. Cada tag pode estar associada à uma ou mais tarefas.
    /tags/id: permite a edição, alteração e remoção de uma tag.
"""