from django.contrib.auth.models import User, Group
from .models import TaskList, Tag, Task
from rest_framework import viewsets, permissions

from .serializers import TaskListSerializer, TagSerializer, TaskSerializer


class TaskListViewSet(viewsets.ModelViewSet):
    queryset = TaskList.objects.filter(active=True).order_by('-name')
    serializer_class = TaskListSerializer
    permission_classes = [permissions.IsAuthenticated]


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.filter(active=True).order_by('-name')
    serializer_class = TagSerializer
    permission_classes = [permissions.IsAuthenticated]


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.filter(active=True).order_by('-title')
    serializer_class = TaskSerializer
    permission_classes = [permissions.IsAuthenticated]