from django.core.exceptions import ValidationError


def validate_field(name, value, valid_values):
    if value not in valid_values:
        raise ValidationError(f'Invalid {name} value ("{value}"). '
                              f'Value should be in ' + str(valid_values))


def validate_activity_type(activity_type):
    validate_field('activity type', activity_type, ('indoors', 'outdoors'))


def validate_status(status):
    validate_field('status', status, ('open', 'done'))
