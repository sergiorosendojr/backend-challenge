from rest_framework import serializers

from .models import TaskList, Tag, Task


class TaskListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TaskList
        fields = ['id', 'name']


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ['id', 'name', 'count']


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ['id', 'title', 'notes', 'priority', 'remind_me_on', 'activity_type', 'status', 'task_list', 'tags']
