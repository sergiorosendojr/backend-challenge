import uuid

from django.db import models

from .validators import validate_activity_type, validate_status


class ApiModel(models.Model):
    active = models.BooleanField(default=True)

    def delete(self, using=None, keep_parents=False):
        self.active = False

    class Meta:
        abstract = True


class TaskList(ApiModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Tag(ApiModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    count = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Task(ApiModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200)
    notes = models.TextField()
    priority = models.IntegerField(default=0)
    remind_me_on = models.DateField(default=None)
    activity_type = models.CharField(max_length=50, validators=[validate_activity_type], blank=False)
    status = models.CharField(max_length=50, validators=[validate_status], blank=False)
    task_list = models.ForeignKey(TaskList, on_delete=models.PROTECT, null=False)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.title
