
"""
    /taskList: retorna todas as lista cadastradas e permite criar uma nova lista. Cada lista possui zero ou mais tarefas.
    /taskList/id: permite a edição, alteração e remoção de uma lista específica
    /tasks: retorna todas as tarefas de uma lista e permite criar uma nova tarefa. Cada tarefa esta sempre associada à uma lista.
    /tasks/id: permite a edição, alteração e remoção de uma tarefa
    /tags: retorna todas as tags cadastradas. Cada tag pode estar associada à uma ou mais tarefas.
    /tags/id: permite a edição, alteração e remoção de uma tag.
"""